# Fastly (Varnish) VCL configuration

Fastly is currently know to be using Varnish 2.1.5.

## Flags

VCL contains certain toggles that can enable some features.

### Enable HTTP Basic Authentication

To enable HTTP Basic Auth with foo as username, and bar as password,
base64 encode a ':' separated username and password string;

    echo -n "contenthub:contenthub" | base64
    Y29udGVudGh1Yjpjb250ZW50aHVi

Then add to Fastly Dashboard configuration in Content tab, at Headers:

    Destination: http.X-Flag-Basic-Auth-Hash
    Source:      "Basic Y29udGVudGh1Yjpjb250ZW50aHVi"


### Wordpress Admin

Allow access to `/wp-admin`.

    Destination: http.X-Flag-Allow-Admin
    Source:      "1"


### XMLRPC calls

Allow calls to XMLRPC `/xmlrpc.xml`.

    Destination: http.X-Flag-Allow-Xmlrpc
    Source:      "1"

### Wordpress Cron

Allow Wordpress Cron `/wp-cron`.

    Destination: http.X-Flag-Allow-Cron
    Source:      "1"

### Allow Wordpress JSON

Allow Wordpress JSON `/wp-json`.

If you want to enable them, add to Fastly Dashboard header:

    Destination: http.X-Flag-Allow-Json
    Source:      "1"

## Script to test URLs

To test the VCL, you can use the script `urls.sh` which will use `httpie` to test a list of urls provided by a file.

1. We need a list of URLs to loop through

    head urls.txt
    /
    /.cpanel_config.php
    /.well-known/apple-app-site-association
    /.well-known/assetlinks.json
    /11.php
    /2010/05/
    /404
    /?s=+96822879


2. Use `urls.sh` and run through the first 3 URLs in `urls.txt`

    COOKIE_STRING=" " SERVICE_HOSTNAME="betastream.co" ORIGIN_IP="151.101.44.204" ./urls.sh urls.txt 3


3. Use `urls.sh` and run through all of them

    COOKIE_STRING=" " SERVICE_HOSTNAME="betastream.co" ORIGIN_IP="151.101.44.204" ./urls.sh urls.txt
