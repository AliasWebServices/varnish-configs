#!/bin/bash
# urls.sh - Run HTTP queries against a list of URLs

#set -e

command -v http >/dev/null 2>&1 || { echo >&2 "I require http command (httpie) but it's not installed.  Aborting. See http://httpie.org/ to get it installed."; exit 1; }

if [[ -z ${1} ]]; then
  echo >&2 "You MUST provide path to a text file as an argument. Each line in it MUST be in the format of one URL path (e.g. /index.html)."; exit 1;
fi

if [[ ! -f ${1} ]]; then
  echo >&2 "No file called ${1} found."; exit 1;
fi

if [[ -z ${SERVICE_HOSTNAME} ]]; then
  echo >&2 "No SERVICE_HOSTNAME shell environment variable set"; exit 1;
  ## e.g.
  ## fish:    set -gx SERVICE_HOSTNAME www.example.org
  ## bash:    export SERVICE_HOSTNAME="www.example.org"
fi

if [[ -z ${ORIGIN_IP} ]]; then
  echo >&2 "No ORIGIN_IP shell environment variable set"; exit 1;
  ## e.g.
  ## fish:    set -gx ORIGIN_IP 172.28.128.7
  ## bash:    export ORIGIN_IP="172.28.128.7"
fi


if [[ -z ${COOKIE_STRING} ]]; then
  echo >&2 "No COOKIE_STRING shell environment variable set"; exit 1;
  ## e.g.
  ## fish:    set -gx COOKIE_STRING "cookie_name=cookievalue;"
  ## bash:    export COOKIE_STRING="cookie_name=cookievalue;"
fi


if [[ ! -z $2 ]]; then
  MAX=$(($2+0))
fi


## Catch Ctrl+C
control_c()
{
  echo -en "\n*** Exiting ***\n"
  exit $?
}
trap control_c SIGINT


iter=0
base=1
for line in `cat ${1}`; do
  if [[ ! -z $MAX ]]; then
    if [[ $iter -ge $MAX ]]; then
      echo "Exit: Reached maximum of $MAX"
      break
    else
      sleep_time=$(( (RANDOM % 8) + 2 ))
      echo "----"
      echo "sleeping: ${sleep_time}"
      sleep ${sleep_time}
    fi
  fi
  (( iter += base ))
  echo "command: http \"http://${ORIGIN_IP}${line}\" Host:${SERVICE_HOSTNAME} Fastly-Debug:1" &&
  echo "iter: $iter" &&
  echo "URL: ${line}" &&
  http http://${ORIGIN_IP}${line} \
            Host:${SERVICE_HOSTNAME} \
            'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
            "Cookie: ${COOKIE_STRING}" \
            "Fastly-Debug:1" \
            --headers &&
  printf "\n\n"
done
