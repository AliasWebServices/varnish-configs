sub vcl_recv {
#FASTLY recv

    ## Header overwrite XFF
    set req.http.X-Fork-Client-IP = client.ip;
    if (req.http.Fastly-Client-IP) {
        set req.http.X-Fork-Client-IP = req.http.Fastly-Client-IP;
    }
    if (req.http.X-Forwarded-For) {
        set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + req.http.X-Fork-Client-IP;
    } else {
        set req.http.X-Forwarded-For = req.http.X-Fork-Client-IP;
    }
    unset req.http.X-Fork-Client-IP;

    if (req.http.X-Flag-Basic-Auth-Hash && req.request != "FASTLYPURGE" && req.http.Authorization != req.http.X-Flag-Basic-Auth-Hash) {
        error 401 "Restricted";
    }

    if (req.http.X-Flag-Allow-Admin) {
        if (req.url ~ "^\/wp-(login|admin)" || req.http.Cookie ~ "wordpress_logged_in_") {
            return (pass);
        }
    } else {
        if (req.url ~ "^\/wp-(login|admin)" && req.url !~ "^\/wp-admin\/(debug|admin-ajax)") {
            set req.url = "/404";
            return (lookup);
        }
    }

    if (!req.http.X-Flag-Allow-Json) {
        if (req.url ~ "\/wp-json\/") {
          set req.url = "/404";
        }
    }

    if (!req.http.X-Flag-Allow-Cron) {
        if (req.url ~ "\/wp-cron\.php") {
          set req.url = "/404";
        }
    }

    if (!req.http.X-Flag-Allow-Xmlrpc) {
      if (req.url ~ "xmlrpc\.php") {
        set req.url = "/404";
      }
    }

    if (req.http.Cookie) {
        set req.http.Cookie = ";" req.http.Cookie;
        set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
        set req.http.Cookie = regsuball(req.http.Cookie, ";(wordpress_[a-z0-9]+|wordpress_test_cookie|wp-settings-[0-9]+|wp-settings-time-[0-9]+|wordpress_logged_in_[a-z0-9]+)=", "; \1=");
        set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
        set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");
        if (req.http.Cookie == "") {
            remove req.http.Cookie;
        }
    }

    # Custom query parameters
    if (
         (!req.url ~ "^\/(page\/[0-9]+\/|)\?s=") &&
         (!req.url ~ "(\/sjr-cse)\/.*(\/search).*([?|&]cse=1)") &&
         (!req.url ~ "\/opt-in") &&
         (!req.url ~ "\?.*post_type=post.*p\=\d{1,11}")
    ) {
        set req.url = regsub(req.url, "\?.*", "");
    }

    # Force lookup if the request is a no-cache request from the client.
    if (req.http.Cache-Control ~ "no-cache") {
        return (pass);
    }

    ## Fastly BOILERPLATE ========
    if (req.request != "HEAD" && req.request != "GET" && req.request != "FASTLYPURGE") {
        return (pass);
    }

    return(lookup);
    ## Fastly BOILERPLATE ========
}

sub vcl_fetch {
    # Handle errors with stale content
    if (beresp.status >= 500 && beresp.status < 600) {
        /* deliver stale if the object is available */
        if (stale.exists) {
            return(deliver_stale);
        }

        if (req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
            restart;
        }

        /* else go to vcl_error to deliver a synthetic */
        error 503;
    }

    /* set stale_if_error and stale_while_revalidate (customize these values) */
    set beresp.stale_if_error = 86400s;
    set beresp.stale_while_revalidate = 60s;

#FASTLY fetch

    if (!beresp.http.X-Url-Orig) {
        set beresp.http.X-Url-Orig = req.url;
    }

    if (!beresp.http.X-Cache-Note) {
        set beresp.http.X-Cache-Note = "Debug notes: At fetch";
    }

    if (beresp.http.Vary ~ "User-Agent") {
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note "; Vary contained user-agent";
        if (beresp.http.Vary ~ "Accept-Encoding") {
            set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note "; Vary contained accept-enconding";
            set beresp.http.Vary = "Accept-Encoding";
        } else {
            set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note "; Vary had been unset";
            unset beresp.http.Vary;
        }
    }

    /**
     * We only want to vary on Accept-Encoding. Vary is not needed on
     * non cacheable objects (i.e. no cookie, no Authentication header,
     * POST responses, etc).
     *
     * Also, WordPress says we should Vary on User-Agent. Every permutation
     * of a browser string would make a different cache object. Heresy.
     *
     * Source Fastly Automatic Gzip [7]
     **/
    if ((beresp.status < 299 || beresp.status == 304 || beresp.status == 404) &&
        req.url ~ "\.(css|js|html|eot|ico|otf|ttf|json|woff|woff2|svg|xml)($|\?)"
    ) {
        # always set vary to make sure uncompressed versions dont always win
        if (!beresp.http.Vary ~ "Accept-Encoding") {
            if (beresp.http.Vary) {
                set beresp.http.Vary = beresp.http.Vary ", Accept-Encoding";
            } else {
                set beresp.http.Vary = "Accept-Encoding";
            }
        }
        if (req.http.Accept-Encoding == "gzip") {
            set beresp.gzip = true;
        }
    }

    if (req.url ~ "^/wp-content") {
        set beresp.ttl = 7d;
    }

    # Remove unneeded headers
    unset beresp.http.Link;
    unset beresp.http.Server;

    ## Lets not cache 401 responses
    if (beresp.status == 401) {
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note ", Had beresp.status to 401"; #DEBUG
        set req.http.Fastly-Cachetype = "UNAUTHORIZED";
        return (pass);
    }

    ## Fastly BOILERPLATE ========
    if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.request == "GET" || req.request == "HEAD")) {
        restart;
    }

    if(req.restarts > 0 ) {
        set beresp.http.Fastly-Restarts = req.restarts;
    }

    if (beresp.http.Set-Cookie && req.url !~ "^\/wp-(login|admin)"){
        unset beresp.http.Set-Cookie;
    }

    if (beresp.http.Cache-Control ~ "private") {
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note ", Cache-Control private"; #DEBUG
        set req.http.Fastly-Cachetype = "PRIVATE";
        return (pass);
    }

    if (beresp.status == 500 || beresp.status == 503) {
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note ", backend returned 50x error"; #DEBUG
        set req.http.Fastly-Cachetype = "ERROR";
        set beresp.ttl = 1s;
        set beresp.grace = 5s;
        return (deliver);
    }

    if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~"(s-maxage|max-age)") {
        # keep the ttl here
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note ", Has either max-age,Expires,Cache-control"; #DEBUG
    } else {
        # apply the default ttl
        set beresp.ttl = 3600s;
        set beresp.http.X-Cache-Note = beresp.http.X-Cache-Note ", Had no max-age,expires,cache-control"; #DEBUG
    }

    return(deliver);
    ## /Fastly BOILERPLATE ========
}

sub vcl_hit {
#FASTLY hit

    ## Fastly BOILERPLATE ========
    if (!obj.cacheable) {
        return(pass);
    }
    return(deliver);
    ## /Fastly BOILERPLATE ========
}

sub vcl_deliver {
#FASTLY deliver

    set resp.http.X-Url-End = req.url; #DEBUG
    if (resp.http.X-Cache-Note) {
        set resp.http.X-Cache-Note = resp.http.X-Cache-Note ", lastuse: " obj.lastuse; #DEBUG
    } else {
        set resp.http.X-Cache-Note = "lastuse: " obj.lastuse; #DEBUG
    }

    # The (!req.http.Fastly-FF) is to differentiate between
    #   edge to the shield nodes. Shield nodes has a Fastly-FF
    #   header added internally.
    if ((!req.http.Fastly-FF) && (!req.http.Fastly-Debug)) {
        unset resp.http.X-Cache-Note;
        unset resp.http.Server;
        unset resp.http.X-Served-By;
        unset resp.http.X-Cache;
        unset resp.http.X-Cache-Hits;
        unset resp.http.X-Timer;
        unset resp.http.X-Flag-Allow-Admin;
        unset resp.http.X-Render-Perf;
        unset resp.http.X-Url-Orig;
        unset resp.http.X-Url-End;
    }

    ## Fastly BOILERPLATE ========
    return(deliver);
    ## /Fastly BOILERPLATE ========
}

sub vcl_error {
#FASTLY error

    if (obj.status >= 500 && obj.status < 600) {
        synthetic {"<!DOCTYPE html><html><head><meta charset=utf-8/><title>Please come back later.</title><style>html{color:#737373;background-color:#f0f0f0;text-align:center;}</style></head><body><h1>Apologies</h1><p>We should be back shortly!</p></body></html>"};
        return(deliver);
    }

    if (obj.status == 401) {
        set obj.http.Content-Type = "text/html; charset=utf-8";
        set obj.http.WWW-Authenticate = "Basic realm=Secured";
        synthetic {"<!DOCTYPE html><html><head>
<title>401 "} obj.response {"</title>
</head><body>
<h1>"} obj.response {"</h1>
<p>This server could not verify that you are authorized to access
this location.</p>
<p>You either supplied the wrong credentials (e.g., bad password), or your
browser doesn't understand how to supply the credentials required.</p>
</body></html>"};
  return (deliver);
  }
    ## Fastly BOILERPLATE ========
    return (deliver);
    ## /Fastly BOILERPLATE ========
}
